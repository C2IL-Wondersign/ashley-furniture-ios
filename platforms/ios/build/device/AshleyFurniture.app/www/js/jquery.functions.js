/**
 * User: tf
 * Date: 23.09.2014
 * Time: 13:37
 */
var ratio = 0;
var idleTime = 0;
var widthHeightRatio = 0;

function getWidthHeightRatio(){
    widthHeightRatio = $(window).width() / $(window).height();
    if(widthHeightRatio <= 1.35){
        $('body').addClass('res4to3');
    } else {
        $('body').removeClass('res4to3');
    }
}

function scaleApp() {
    var ww = $(window).width();
     var mw = 1366;
     // min width of site
     ratio = ww / mw;
     $('#wrapper')
     .css({
     '-webkit-transform': 'scale(' + ratio + ')',
     '-moz-transform': 'scale(' + ratio + ')',
     '-ms-transform': 'scale(' + ratio + ')',
     '-o-transform': 'scale(' + ratio + ')',
     'transform': 'scale(' + ratio + ')'
     });
}

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 3) { // 15 sec
        $('#touchOverlay, #touchPulse, #touchPointer').fadeIn();
    }
}

function setIdleToZero() {
    idleTime = 0;
    $('#touchOverlay, #touchPulse, #touchPointer').hide();
}

function showGridView(){
    hideCatalog();
    hideSearch();
   /* $('.homebutton').hide();*/
    $('.view').removeClass('active');
    $('.gridView').addClass('active');
}

function showDetailView(){
    hideCatalog();
    hideSearch();
    $('.homebutton').show();
    $('.view').removeClass('active');
    $('.detailView').addClass('active');

}

function showSearchView(){
    hideCatalog();
    hideSearch();
    $('.homebutton').show();
    $('.view').removeClass('active');
    $('.searchView').addClass('active');
}

function toggleCatalog(){
    hideSearch();
    $('#catalog, .catalog').toggleClass('active');
}

function hideCatalog(){
    $('#catalog, .catalog').removeClass('active');
}

function toggleSearch(){
    hideCatalog();
    $('#searchField, .searchButton').toggleClass('active');
    $('#searchField.active #searchInput').focus();
}

function hideSearch(){
    $('#searchField, .searchButton').removeClass('active');
    $('#searchInput').blur();
}

function setContentDimensions() {
    $('#catalog').height($('#wrapper').height() - $('.pageHeader').height()-parseInt($('#catalog').css('padding-top')) -parseInt($('#catalog').css('padding-bottom')) );
    $('#catalog, #searchField').css('top',$('.pageHeader').height());
    $('.pageContent').height($('#wrapper').height() - $('.pageHeader').height() - parseInt($('.pageContent').css('padding-top')) - parseInt($('.pageContent').css('padding-bottom')) - parseInt($('.pageContent').css('margin-top')) - parseInt($('.pageContent').css('margin-bottom')));
}

$(document).ready(function () {
    getWidthHeightRatio();
    setContentDimensions();
    scaleApp();
    showGridView();

     /* $('#productGridSlider .productGridImage').each(function () {
        $(this).css('background-image','url('+$(this).attr('data-image')+')');
    });*/

$('#searchInput').keypress(function(event){

//alert("search in search input");

                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                      var searchTag = $("#searchInput").val();
                      if(searchTag == "" || searchTag == " " || searchTag == null )
                        {
                            alert("Search input cannot be blank");
                        }
                        else
                        {
                  localStorage.setItem("search_tag",searchTag);
                  $(location).attr("href","search.html")
                        }
                }
               // else{
                 //   alert("error");
                //}
                //Stop the event from propogation to other handlers
                //If this line will be removed, then keypress event handler attached
                //at document level will also be triggered
                event.stopPropagation();
            });


$("#search").click(function(){

   var searchTag = $("#searchInput").val();
if(searchTag == "" || searchTag == " " || searchTag == null )
{
    alert("Search input cannot be blank");
}
else
{

  localStorage.setItem("search_tag",searchTag);

$(location).attr("href","search.html")
}

})


    $('.homebutton, .logo').on('tap', function () {
        showGridView();
    });

    /*$('.box').on('tap', function () {
        showDetailView();
    });*/
	
	$('.box1').on('tap', function () {
        showDetailView();
    });

    $('.catalog').on('click', function () {
        


        /*toggleCatalog();*/

/*alert("Function currently disabled");*/
                    // $('.categoryList > ul > li > a').each(function(){

                    //         if($(this).parent().find('ul').length > 0) $(this).addClass('hasSub');

                    //         $(this).on('tap', function () {
                    //             if($(this).hasClass('active')){
                    //                 // do nothing
                    //             } else {
                    //                 $('.categoryList ul ul').slideUp();
                    //                 $('.categoryList > ul > li > a').removeClass('active');
                    //                 $(this).addClass('active').parent().find('ul').slideDown();
                    //             }
                    //         });
                    //     });
         toggleCatalog();

    });

    $(".page").on('click', 'a.hasSub', function(event) 
        {
           
            var parentid=$(this).closest("li").attr("id");
            var parentclass=$(this).closest("li").attr("class");
             /*alert("clicked"+parentid+"class.."+parentclass);*/
             /*var isVisible = $( '.maincatul #'+par ).is( ".opened" );*/
             if(parentclass=="maincatul")
             {
                var isopened=$(this).parent().find('ul').is( ".opened" );
                /*alert("is opened.."+isopened);*/
                if(isopened==false)
                {
                $('.categoryList ul ul').slideUp();
               
                }
                else if(isopened==true)
                {
                     $('.categoryList > ul > li > a').removeClass('active');
                }
             }
             /*$('.categoryList ul ul').not("a.hasSub li#"+parentid+" ul li ul")slideUp();*/
             
            $(this).addClass('active').parent().find('ul').not("li#"+parentid+" ul li ul").slideToggle().toggleClass('opened');
        });

    $('.additionalImage').on('tap', function () {
        $('#detailMainImage').css('background-image','url(../images/dummy03.jpg)');
    });

    $('.searchButton').on('click', function () {
        toggleSearch();
       
    });

    $('.sortLink').on('tap', function () {
        $('.sortLink').removeClass('active');
        $(this).addClass('active');
    });

    $('#sortByStyle').on('tap', function () {
        $('#catalogCategories').addClass('styles');
    });

    $('#sortByRoom').on('tap', function () {
        $('#catalogCategories').removeClass('styles');
    });

    $('.categoryList a').on('tap', function () {
        $(this).closest('ul').find('a').removeClass('activeColor');
        $(this).addClass('activeColor');
    });



    $('.categoryList a').not('.hasSub').on('tap', function(){
        setTimeout(hideCatalog,200);
    });

    $('h2').on('tap', function () {
        if($(this).hasClass('active')){
            // do nothing
        } else {
            $('h2').removeClass('active').next().slideUp();
            $(this).addClass('active').next().slideDown();
        }
    }).next().hide();
    $('h2:first').addClass('active').next().show();

/*
    $('#callMailPopup').on('tap', function () {

    });

    $('#callMobilePopup').on('tap', function () {

    });

    $('#callAssistancePopup').on('tap', function () {

    });
*/

    var idleInterval = setInterval(timerIncrement, 5000);
    $(window).on('tap touchstart touchend touchmove click', function () {
        setIdleToZero();
    });
});

$(window).resize(function () {
    setContentDimensions();
    scaleApp();
    getWidthHeightRatio();
});

$(window).on("orientationchange", function (event) {
    setContentDimensions();
    scaleApp();
    getWidthHeightRatio();
});
